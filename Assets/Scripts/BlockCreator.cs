﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCreator : MonoBehaviour {

    public GameObject bridgePrefab;
    Vector3 mousePos;
    Camera _camera;
    // Use this for initialization
    void Start() {
        _camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update() {
        mousePos = Input.mousePosition;

        

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = _camera.ScreenPointToRay(mousePos);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                Debug.Log("HIT!");
                if(hit.collider.tag == "BlockGenerator")
                {
                    StartCoroutine(BlockGenerator(hit.point));
                }
                return;
            }
        }


    }

    private IEnumerator BlockGenerator(Vector3 pos)
    {
        GameObject bridge = GameObject.Instantiate(bridgePrefab);
        bridge.transform.position = pos;

        yield return new WaitForSeconds(3);

        Destroy(bridge);
    }
}
