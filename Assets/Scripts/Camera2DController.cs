﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2DController : MonoBehaviour {

    GameObject player;
    private Vector3 cameraSpeed;
    public float camSmooth;
    public float zOffset = 10;
    

    CharControl charControl;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        charControl = Object.FindObjectOfType<CharControl>();
         
    }
    void Update()
    {
        Vector3 newPosition = player.transform.position;
        cameraSpeed = Vector3.zero;

        newPosition.z -= zOffset;

        if (charControl.charMovement.magnitude < 7)
        {
            transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref cameraSpeed, camSmooth);
        }
        else
        {

            newPosition.x -= 1;
            
            transform.position = newPosition;
        }
    }
}
