﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[AddComponentMenu("charController")]
[RequireComponent(typeof(Rigidbody))]
public class CharControl : MonoBehaviour
{

    Rigidbody rb;
    Collider col;
    GameObject groundCheck;

    public Vector3 charMovement;

    public float speedOfMovement;
    public float jumpForce;

    public bool isGrounded = false;

    void Start()
    {
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        groundCheck = GameObject.Find("GroundCheck");
    }

    // Update is called once per frame
    void Update()
    {
        charMovement = rb.velocity;

        isGrounded = IsGrounded();


        float directionOfMovement = Input.GetAxis("Horizontal");
        charMovement.x = directionOfMovement * speedOfMovement;



        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            charMovement.y = jumpForce;
        }
        rb.velocity = charMovement;
       // Debug.Log(rb.velocity);

    }
    bool IsGrounded()
    {
        Ray ray = new Ray(groundCheck.transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
            if (hit.distance < 0.05f && hit.collider.tag == "Ground")
                return true;

        return false;
    }
}
